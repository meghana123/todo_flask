from flask import Flask,render_template

app=Flask(__name__)

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/gech')
def file():
	return render_template('file.html')

@app.route('/ok')
def file1():
	return render_template('file1.html')

@app.route('/abc')
def abc():
	return render_template('abc.html')


if __name__=='__main__':
	app.run(debug=True)
